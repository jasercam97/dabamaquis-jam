﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource AS;

    public AudioClip Raphael;
    public AudioClip Chayanne;

    public static MusicManager Instance;

    private void Awake()
    {
        Instance = this;
        AS.volume = 0;
    }

    private void Update()
    {
        if (GameManager.Instance.GetGameState() == false) { return; }
    }

    public void AddVolume()
    {
        AS.volume += 0.15f;
    }

    public void SubtractVolume()
    {
        AS.volume -= 0.15f;
    }

    public void SetChayanne()
    {
        AS.clip = Chayanne;
        Debug.Log("hi");
    }

    public void SetRaphael()
    {
        AS.clip = Raphael;
    }
}
