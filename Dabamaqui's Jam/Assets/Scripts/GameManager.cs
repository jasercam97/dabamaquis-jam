﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool GameStarted;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else { Destroy(this); }
    }

    private void Update()
    {
        if (!GameObject.FindObjectOfType<MouseController>()) { Invoke("EndGame", 1.5f); }
    }

    public bool GetGameState()
    {
        return GameStarted;
    }

    public void StartGame()
    {
        GameStarted = true;
    }

    void EndGame()
    {
        SceneManager.LoadScene(0);
    }
}
