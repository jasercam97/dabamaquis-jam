﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleManager : MonoBehaviour
{
    public List<GameObject> Circles;
    public float TimeChange = 15f;
    public int CirclesActivated = 5;

    void Start()
    {
        
        InvokeRepeating(nameof(ChangeCircles), 0f, TimeChange);
    }

    private void ChangeCircles()
    {
        foreach (GameObject g in Circles)
        {
            g.GetComponentInChildren<CircleBehavior>().Deactivate();
        }

        for(int i = 0; i <= 5; i++)
        {
            var rand = Random.Range(0, Circles.Count - 1);

            Circles[rand].GetComponentInChildren<CircleBehavior>().Activate();
        }
    }
}
