﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleBehavior : MonoBehaviour
{
    public Material ActivatedMat;
    public Material DeactivatedMat;
    public Material DancingMat;
    public GameObject AuraDancing;

    private bool _activated;
    private bool _dancing;
    private SpriteRenderer _mat;
    // Start is called before the first frame update
    void Start()
    {
        _mat = GetComponent<SpriteRenderer>();
        AuraDancing.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GetGameState() == false) { return; }
        if (_activated && _dancing)
        {
            ScoreManager.Instance.AddScore(1);
        }
    }

    public void Activate()
    {
        _activated = true;
        _mat.sharedMaterial = ActivatedMat;

        Physics2D.OverlapBox(transform.position, Vector2.one, 0f);
    }

    public void Deactivate()
    {
        _activated = false;
        _dancing = false;
        _mat.sharedMaterial = DeactivatedMat;
        AuraDancing.SetActive(false);

        if(_dancing)
        {
            MusicManager.Instance.SubtractVolume();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(_activated && collision.CompareTag("Player"))
        {
            _dancing = true;
            _mat.sharedMaterial = DancingMat;
            AuraDancing.SetActive(true);
            MusicManager.Instance.AddVolume();

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _dancing = false;
        AuraDancing.SetActive(false);
        MusicManager.Instance.SubtractVolume();
        if (_activated)
            _mat.sharedMaterial = ActivatedMat;
        else
            _mat.sharedMaterial = DeactivatedMat;
    }

    public bool isActivated()
    {
        return _activated;
    }
}
