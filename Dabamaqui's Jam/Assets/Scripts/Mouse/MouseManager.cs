﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    public List<MouseActivator> ListOfMouses;

    private void Update()
    {
        if(GameManager.Instance.GetGameState() == false) { return; }

        if (Input.GetKeyDown(ListOfMouses[0].ActivationKey))
        {
            ActivateMouse(ListOfMouses[0].Mouse);
        }
        
        else if (Input.GetKeyDown(ListOfMouses[1].ActivationKey))
        {
            ActivateMouse(ListOfMouses[1].Mouse);
        }
        
        else if (Input.GetKeyDown(ListOfMouses[2].ActivationKey))
        {
            ActivateMouse(ListOfMouses[2].Mouse);
        }

        else if (Input.GetKeyDown(ListOfMouses[3].ActivationKey))
        {
            ActivateMouse(ListOfMouses[3].Mouse);
        }
    }

    void ActivateMouse(MouseController mouse)
    {
        foreach (MouseActivator m in ListOfMouses)
        {
            if (m.Mouse == mouse) { m.Mouse.SetMouseState(true); }
            else { m.Mouse.SetMouseState(false); }
        }
    }
}
