﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public float MoveSpeed;

    public Material NormalMaterial;
    public Material OutlineMaterial;

    private float _hValue;
    private float _vValue;

    private bool _isActive;
    private bool _isDancing;

    private Animator _anim;
    private SpriteRenderer _renderer;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (GameManager.Instance.GetGameState() == false) { return; }

        if (_isActive) { DoMove(); }
        else { SetDisabledColor(); }
    }  

    private void DoMove()
    {
        _renderer.color = Color.white;
        _renderer.sharedMaterial = OutlineMaterial;

        _hValue = Input.GetAxisRaw("Horizontal");
        _vValue = Input.GetAxisRaw("Vertical");

        _anim.SetFloat("H", _hValue);
        _anim.SetFloat("V", _vValue);

        if (_hValue != 0 && _vValue == 0)
        {
            transform.Translate(Vector2.right * _hValue * MoveSpeed * Time.deltaTime);

            if (_hValue < 0) { _renderer.flipX = true; }
            else if (_hValue > 0) { _renderer.flipX = false; }
        }

        else if (_hValue == 0 && _vValue != 0)
        {
            transform.Translate(Vector2.up * _vValue * MoveSpeed * Time.deltaTime);
        }
        else if (_isDancing)
        {
            _anim.Play("Dance");
        }
        else { _anim.Play("Idle"); }
    }

    private void SetDisabledColor()
    {
        _renderer.color = Color.gray;
        _renderer.sharedMaterial = NormalMaterial;
    }

    [ContextMenu("Test")]
    private void SetDanceAnimation()
    {
        _isDancing = true;
    }

    public void SetMouseState(bool state) 
    {
        _isActive = state;
    }

    void DanceFlipEvent()
    {
        _renderer.flipX = !_renderer.flipX;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Circle")) { _isDancing = true; }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Circle")) { _isDancing = false; }
    }

}
