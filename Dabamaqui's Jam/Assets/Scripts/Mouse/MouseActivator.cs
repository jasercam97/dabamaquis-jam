﻿using UnityEngine;

[System.Serializable]
public class MouseActivator
{
    public MouseController Mouse;
    public KeyCode ActivationKey;
}
