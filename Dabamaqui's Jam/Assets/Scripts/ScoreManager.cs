﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI ScoreText;
    public static ScoreManager Instance;

    private int _score;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {

        SetScore(0);
    }

    public void SetScore(int value)
    {
        _score = value;
        UpdateHUD();
    }

    public void AddScore(int value)
    {
        _score += value;
        UpdateHUD();
    }

    private void UpdateHUD()
    {
        ScoreText.text = "Score: " + _score;
    }
}
