﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CatIA : MonoBehaviour
{
    private const float MAX_PERSECUTION_TIME = 0.5f;
    public enum CatState
    {
        Patrol, Persecution
    }
    [SerializeField] private float speedMovement = 2.5f;
    [SerializeField] private List<Transform> patrolPoints;
    [SerializeField] private LayerMask layerRayCast;
    [SerializeField] private float visionDistance = 5f;

    public bool isOver;

    private int _currentPatrolIndex;
    public Vector2 _moveDirection;
    private CatState _state;
    private float persecutionTime;
    public Animator _anim;
    public SpriteRenderer _renderer;

    private void Awake()
    {
        persecutionTime = 0;
        _currentPatrolIndex = 0;
        _moveDirection = (patrolPoints[_currentPatrolIndex].position - transform.position).normalized;
        _moveDirection = new Vector2(Mathf.CeilToInt(_moveDirection.x), Mathf.RoundToInt(_moveDirection.y));
        isOver = false;
    }

    private void Update()
    {
        if (GameManager.Instance.GetGameState() == false) { return; }

        if (!isOver)
        {
            FiniteStateMachine();
            transform.position = transform.position + (Vector3)_moveDirection* speedMovement * Time.deltaTime;
        }
        if(_moveDirection == Vector2.right)
        {
            _anim.SetFloat("H", 1);
            _anim.SetFloat("V", 0);

            _renderer.flipX = true;
        }
        if (_moveDirection == Vector2.left)
        {
            _anim.SetFloat("H", -1);
            _anim.SetFloat("V", 0);

            _renderer.flipX = false;
        }
        if (_moveDirection == Vector2.up)
        {
            _anim.SetFloat("H", 0);
            _anim.SetFloat("V", 1);
        }
        if (_moveDirection == Vector2.down)
        {
            _anim.SetFloat("H", 0);
            _anim.SetFloat("V", -1);
        }
    }

    private void FiniteStateMachine()
    {
        switch (_state)
        {
            case CatState.Patrol:
                DoPatrol();
                break;
            case CatState.Persecution:
                DoPersecution();
                break;
            default:
                DoPatrol();
                break;
        }
    }
    private void DoPatrol()
    {
        //Primero comprueba si puede ver al jugador

           
            if (CanSeePlayer())
            {
                Debug.Log("Veo al jugador");
                _state = CatState.Persecution;
            }
            //En caso negativo, se mueve hacía el siguiente punto de patrulla y sigue con el estado de patrulla
            else
            {
                _moveDirection = (patrolPoints[_currentPatrolIndex].position - transform.position).normalized;
                _moveDirection = new Vector2(Mathf.RoundToInt(_moveDirection.x), Mathf.RoundToInt(_moveDirection.y));
                if (Vector3.Distance(transform.position, patrolPoints[_currentPatrolIndex].position) < 0.05f)
                {
                    _currentPatrolIndex++;
                    if (_currentPatrolIndex >= patrolPoints.Count)
                    {
                        _currentPatrolIndex = 0;
                    }

                }
            }


    }
    private void DoPersecution()
    {
        if (persecutionTime >= MAX_PERSECUTION_TIME)
        {
            persecutionTime = 0;
            //Si puede ver al jugador lo persigue mirando hacia el
            if (!CanSeePlayer())
            {
                Debug.Log("Dejo de ver al jugador");

                _state = CatState.Patrol;
            }
        }
        persecutionTime += Time.deltaTime;
    }
    /// <summary>
    /// Módulo encargado de comprobar si puede ver al jugador en un radio y angulo determinado. Si hay un muro entre el jugador
    /// y el oso, no podrá ver al jugador
    /// </summary>
    /// <returns>True si lo ve, false si no</returns>
    private bool CanSeePlayer()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, _moveDirection, visionDistance, layerRayCast);
        if (hit)
        {
            Debug.Log(hit.collider.name);
            if (hit.transform.CompareTag("Player"))
            {
                _moveDirection = (hit.transform.position- transform.position).normalized;
               // _moveDirection = new Vector2(Mathf.RoundToInt(_moveDirection.x), Mathf.RoundToInt(_moveDirection.y));
                return true;
            }
        }
        return false;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            col.gameObject.SetActive(false);
        }
    }

#if UNITY_EDITOR
    /// <summary>
    /// Para ver en el editor la distancia de visión y de escuchar
    /// </summary>
    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, _moveDirection * visionDistance, Color.red);
    }
#endif
}
